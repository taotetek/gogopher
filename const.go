package gogopher

// GopherTypeText represents a text file
const GopherTypeText = "0"

// GopherTypeDirectory represents a directory
const GopherTypeDirectory = "1"

// GopherTypeError represents an error
const GopherTypeError = "3"

// GopherTypeZip represents a zip file
const GopherTypeZip = "5"

// GopherTypeUUEncoded represents a uuencoded file
const GopherTypeUUEncoded = "6"

// GopherTypeIndexSearch represents an index search system
const GopherTypeIndexSearch = "7"

// GopherTypeTelnet represents a telnet connection
const GopherTypeTelnet = "8"

// GopherTypeBinary represents a generic binary file
const GopherTypeBinary = "9"

// GopherTypeGif represents a gif file
const GopherTypeGif = "g"

// GopherTypeSound represents an audio file
const GopherTypeSound = "s"

// GopherTypeHTML represents an html file
const GopherTypeHTML = "h"
