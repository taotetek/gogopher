package gogopher

import (
	"path/filepath"
	"testing"
)

func TestNewGopherDirWithGopherMap(t *testing.T) {
	path, _ := filepath.Abs("./test_fixtures/test_gophermap")
	path = "file://" + path
	_, err := NewGopherDir(path)

	if err != nil {
		t.Fatal(err)
	}
}

func TestNewGopherDirNoGopherMap(t *testing.T) {
	path, _ := filepath.Abs("./test_fixtures/test_nogophermap")
	path = "file://" + path

	_, err := NewGopherDir(path)

	if err != nil {
		t.Fatal(err)
	}
}
