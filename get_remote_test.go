package gogopher

import (
	"net/url"
	"testing"
)

func TestLoadGopherDirRemote(t *testing.T) {
	resource, _ := url.Parse("mock://gopher.quux.org:70")
	_, err := LoadGopherDirRemote(resource)
	if err != nil {
		t.Fatal(err)
	}
}
