package main

import (
	"fmt"
	"github.com/taotetek/gogopher"
	"log"
)

func main() {
	gd, err := gogopher.NewGopherDir("gopher://gopher.meatspac.es:70")
	if err != nil {
		log.Fatal(err)
	}

	jsonBytes, err := gd.ToJSON()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(jsonBytes))
}
