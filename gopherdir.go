package gogopher

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
)

// GopherDir represents a single directory of the gopher server
type GopherDir struct {
	Path      string
	GopherMap []GopherMapEntry
}

// NewGopherDir returns a GopherDir instance for given path
func NewGopherDir(path string) (*GopherDir, error) {
	entries, err := LoadGopherDir(path)
	if err != nil {
		return new(GopherDir), err
	}
	dir := &GopherDir{Path: path, GopherMap: make([]GopherMapEntry, len(entries))}
	dir.GopherMap = entries
	return dir, nil
}

// ToResponse returns the string to send to the client
func (g *GopherDir) ToResponse() []byte {
	var buffer bytes.Buffer
	for i := 0; i < len(g.GopherMap); i++ {
		fmt.Fprintf(&buffer,
			"%s%s\t%s\t%s\t%s\n",
			g.GopherMap[i].ItemType,
			g.GopherMap[i].DisplayString,
			g.GopherMap[i].Selector,
			g.GopherMap[i].HostName,
			g.GopherMap[i].Port,
		)
	}
	return buffer.Bytes()

}

// ToJSON returns the GopherDir as JSON bytes
func (g *GopherDir) ToJSON() ([]byte, error) {
	jsonBytes, err := json.Marshal(g)
	return jsonBytes, err
}

// LoadGopherDir loads a gopher directory. It can take either a file:/// or gopher:/// url
func LoadGopherDir(resource string) ([]GopherMapEntry, error) {
	var err error
	var entries []GopherMapEntry

	u, err := url.Parse(resource)
	if err != nil {
		return entries, err
	}

	if u.Scheme == "file" {
		entries, err := LoadGopherDirLocal(u)
		return entries, err
	} else if u.Scheme == "gopher" {
		entries, err := LoadGopherDirRemote(u)
		return entries, err
	} else {
		err = errors.New("bad url")
		return entries, err
	}
}
