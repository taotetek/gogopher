# gogopher
--
    import "github.com/taotetek/gogopher"


## Usage

```go
const GopherTypeBinary = "9"
```
GopherTypeBinary represents a generic binary file

```go
const GopherTypeDirectory = "1"
```
GopherTypeDirectory represents a directory

```go
const GopherTypeError = "3"
```
GopherTypeError represents an error

```go
const GopherTypeGif = "g"
```
GopherTypeGif represents a gif file

```go
const GopherTypeHTML = "h"
```
GopherTypeHTML represents an html file

```go
const GopherTypeIndexSearch = "7"
```
GopherTypeIndexSearch represents an index search system

```go
const GopherTypeSound = "s"
```
GopherTypeSound represents an audio file

```go
const GopherTypeTelnet = "8"
```
GopherTypeTelnet represents a telnet connection

```go
const GopherTypeText = "0"
```
GopherTypeText represents a text file

```go
const GopherTypeUUEncoded = "6"
```
GopherTypeUUEncoded represents a uuencoded file

```go
const GopherTypeZip = "5"
```
GopherTypeZip represents a zip file

#### func  LoadGopherDir

```go
func LoadGopherDir(resource string) ([]GopherMapEntry, error)
```
LoadGopherDir loads a gopher directory. It can take either a file:/// or
gopher:/// url

#### func  LoadGopherDirLocal

```go
func LoadGopherDirLocal(resource *url.URL) ([]GopherMapEntry, error)
```
LoadGopherDirLocal loads a gopher directory from local filesystem

#### func  LoadGopherDirRemote

```go
func LoadGopherDirRemote(resource *url.URL) ([]GopherMapEntry, error)
```

#### type GopherDir

```go
type GopherDir struct {
	Path      string
	GopherMap []GopherMapEntry
}
```

GopherDir represents a single directory of the gopher server

#### func  NewGopherDir

```go
func NewGopherDir(path string) (*GopherDir, error)
```
NewGopherDir returns a GopherDir instance for given path

#### func (*GopherDir) ToJSON

```go
func (g *GopherDir) ToJSON() ([]byte, error)
```
ToJSON returns the GopherDir as JSON bytes

#### func (*GopherDir) ToResponse

```go
func (g *GopherDir) ToResponse() []byte
```
ToResponse returns the string to send to the client

#### type GopherMapEntry

```go
type GopherMapEntry struct {
	ItemType      string
	DisplayString string
	Selector      string
	HostName      string
	Port          string
}
```

GopherMapEntry represents a single entry in a gophermap file
