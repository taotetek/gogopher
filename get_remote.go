package gogopher

import (
	"bytes"
	"errors"
	"net"
	"net/url"
	"time"
)

func makeMockResponse() []byte {
	buf1 := "GoGopher Mock Response!\r\n"
	buf2 := " \r\n"
	buf3 := "0GoGopher Documentation	doc.txt	gopher.meatspac.es	70\r\n"
	buf4 := "1The Gopher Project	/Software/Gopher	gopher.quux.org	70\r\n"
	buf5 := "1Meatspace Gopherspace	/	gopher.meatspac.es	70\r\n"
	buf := buf1 + buf2 + buf3 + buf4 + buf5
	return []byte(buf)
}

func LoadGopherDirRemote(resource *url.URL) ([]GopherMapEntry, error) {
	var err error
	var entries []GopherMapEntry
	reply := make([]byte, 262144)

	if resource.Scheme == "mock" {
		reply = makeMockResponse()
	} else if resource.Scheme == "gopher" {
		tcpAddr, err := net.ResolveTCPAddr("tcp", resource.Host)
		if err != nil {
			return entries, err
		}

		conn, err := net.DialTCP("tcp", nil, tcpAddr)
		if err != nil {
			return entries, err
		}

		conn.SetReadDeadline(time.Now().Add(time.Second * 5))

		_, err = conn.Write([]byte("/\r\n"))
		for {
			n, err := conn.Read(reply)
			if err != nil || n == 0 {
				conn.Close()
				break
			}
		}
		reply = bytes.Trim(reply, "\x00")
	} else {
		err = errors.New("not a gopher url")
	}
	entries, err = parseGopherData(reply)
	return entries, err
}
