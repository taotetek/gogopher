package gogopher

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

func getEntriesNoMap(path string) ([]byte, error) {
	var buffer bytes.Buffer
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return buffer.Bytes(), err
	}
	for _, f := range files {
		fpath := filepath.Join(path, f.Name())
		finfo, err := os.Stat(fpath)
		if err != nil {
			continue
		}
		if !finfo.IsDir() {
			fmt.Fprintf(&buffer,
				"0%s\t%s\tlocalhost\t70\r\n",
				f.Name(),
				f.Name(),
			)
		}
	}
	return buffer.Bytes(), err
}

func getEntriesFromMap(f *os.File) ([]byte, error) {
	var buffer bytes.Buffer
	var err error
	r := bufio.NewReader(f)
	for {
		l, _, err := r.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			break
		}

		parts := strings.Split(string(l), "\t")
		if len(parts) == 4 {
			fmt.Fprintf(&buffer,
				"%s%s\t%s\t%s\t%s\r\n",
				string(parts[0][0]),
				parts[0][1:],
				parts[1],
				parts[2],
				string(parts[3]),
			)
			if err != nil {
				break
			}
		} else {
			fmt.Fprintf(&buffer,
				"i%s\tfake\t(NULL)\t0\r\n",
				string(l),
			)
		}
	}
	return buffer.Bytes(), err
}

// LoadGopherDirLocal loads a gopher directory from local filesystem
func LoadGopherDirLocal(resource *url.URL) ([]GopherMapEntry, error) {
	var gopherData []byte
	var entries []GopherMapEntry
	gopherMapPath := filepath.Join(resource.Path, "gophermap")

	finfo, err := os.Stat(resource.Path)

	if err != nil || !finfo.IsDir() {
		return entries, err
	}

	f, err := os.Open(gopherMapPath)
	defer f.Close()
	if err != nil {
		if os.IsNotExist(err) {
			gopherData, err = getEntriesNoMap(resource.Path)
			entries, err = parseGopherData(gopherData)
			return entries, err
		} else {
			return entries, err
		}
	} else {
		gopherData, err = getEntriesFromMap(f)
		entries, err = parseGopherData(gopherData)
	}
	return entries, err
}
