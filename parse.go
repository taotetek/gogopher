package gogopher

import (
	"strings"
)

func parseGopherData(gopherData []byte) ([]GopherMapEntry, error) {
	var entries []GopherMapEntry
	var err error
	stringRep := string(gopherData)
	parts := strings.Split(stringRep, "\r\n")
	for i := 0; i < len(parts); i++ {
		pieces := strings.Split(parts[i], "\t")
		e := new(GopherMapEntry)
		if len(pieces) >= 4 {
			e.ItemType = string(pieces[0][0])
			e.DisplayString = pieces[0][1:]
			e.Selector = pieces[1]
			e.HostName = pieces[2]
			e.Port = string(pieces[3])
			entries = append(entries, *e)
		} else if len(parts[i]) > 0 {
			e.ItemType = "i"
			e.DisplayString = string(parts[i])
			e.Selector = "fake"
			e.HostName = "(NULL)"
			e.Port = "0"
			entries = append(entries, *e)
		}

	}
	return entries, err
}
