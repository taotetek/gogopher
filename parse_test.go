package gogopher

/*
"GoGopher Mock Response!\r\n"
" \r\n"
"0GoGopher Documentation	doc.txt	gopher.meatspac.es	70\r\n"
"1The Gopher Project	/Software/Gopher	gopher.quux.org	70\r\n"
"1Meatspace Gopherspace	/	gopher.meatspac.es	70\r\n"
*/

import (
	"testing"
)

func TestParse(t *testing.T) {
	buf := makeMockResponse()
	entries, err := parseGopherData(buf)

	if err != nil {
		t.Fatalf("%v", err)
	}

	// first line
	if entries[0].ItemType != "i" {
		t.Fatal("Type for non entry should be i")
	}

	if entries[0].DisplayString != "GoGopher Mock Response!" {
		t.Fatal("Display string is incorrect for first entry")
	}

	if entries[0].Selector != "fake" {
		t.Fatal("Selector for non entry should be fake")
	}

	if entries[0].HostName != "(NULL)" {
		t.Fatal("Hostname for non entry should be (NULL)")
	}

	if entries[0].Port != "0" {
		t.Fatal("Port for non entry should be 0")
	}

	// second line
	if entries[1].ItemType != "i" {
		t.Fatal("Type for non entry should be i")
	}

	if entries[1].DisplayString != " " {
		t.Fatal("Display string is incorrect for second entry")
	}

	if entries[1].Selector != "fake" {
		t.Fatal("Selector for non entry should be fake")
	}

	if entries[1].HostName != "(NULL)" {
		t.Fatal("Hostname for non entry should be (NULL)")
	}

	if entries[1].Port != "0" {
		t.Fatal("Port for non entry should be 0")
	}

}
