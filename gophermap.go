package gogopher

// GopherMapEntry represents a single entry in a gophermap file
type GopherMapEntry struct {
	ItemType      string
	DisplayString string
	Selector      string
	HostName      string
	Port          string
}
