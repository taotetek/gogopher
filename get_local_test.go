package gogopher

import (
	"net/url"
	"path/filepath"
	"testing"
)

func TestLoadGopherDirLocalWithMap(t *testing.T) {
	path, _ := filepath.Abs("./test_fixtures/test_gophermap")
	resource, _ := url.Parse(path)
	entries, err := LoadGopherDirLocal(resource)
	if err != nil {
		t.Fatal(err)
	}

	// first line
	if entries[0].ItemType != "i" {
		t.Fatal("Type for non entry should be i")
	}

	if entries[0].DisplayString != "Welcome to GoGopher!" {
		t.Fatal("Display string is incorrect for first entry")
	}

	if entries[0].Selector != "fake" {
		t.Fatal("Selector for non entry should be fake")
	}

	if entries[0].HostName != "(NULL)" {
		t.Fatal("Hostname for non entry should be (NULL)")
	}

	if entries[0].Port != "0" {
		t.Fatal("Port for non entry should be 0")
	}

	// second line
	if entries[1].ItemType != "i" {
		t.Fatal("Type for non entry should be i")
	}

	if entries[1].DisplayString != "" {
		t.Fatal("Display string is incorrect for second entry")
	}

	if entries[1].Selector != "fake" {
		t.Fatal("Selector for non entry should be fake")
	}

	if entries[1].HostName != "(NULL)" {
		t.Fatal("Hostname for non entry should be (NULL)")
	}

	if entries[1].Port != "0" {
		t.Fatal("Port for non entry should be 0")
	}

	// third line
	if entries[2].ItemType != GopherTypeText {
		t.Fatal("Type for third line should be text")
	}

	if entries[2].DisplayString != "GoGopher Documentation" {
		t.Fatal("Display string is incorrect for third entry")
	}

	if entries[2].Selector != "doc.txt" {
		t.Fatal("Selector is incorrect for third entry")
	}

	if entries[2].HostName != "gopher.meatspac.es" {
		t.Fatal("Hostname is incorrect for third entry")
	}

	if entries[2].Port != "70" {
		t.Fatal("Port for third entry is incorrect")
	}

	// fourth line
	if entries[3].ItemType != GopherTypeDirectory {
		t.Fatal("Type for fourth line should be directory")
	}

	if entries[3].DisplayString != "The Gopher Project" {
		t.Fatal("Display string is incorrect for fourth entry")
	}

	if entries[3].Selector != "/Software/Gopher" {
		t.Fatal("Selector is incorrect for fourth entry")
	}

	if entries[3].HostName != "gopher.quux.org" {
		t.Fatal("Hostname is incorrect for fourth entry")
	}

	if entries[3].Port != "70" {
		t.Fatal("Port for fourth entry is incorrect")
	}
}

func TestLoadGopherDirLocalNoMap(t *testing.T) {
	path, _ := filepath.Abs("./test_fixtures/test_nogophermap")
	resource, _ := url.Parse(path)
	entries, err := LoadGopherDirLocal(resource)
	if err != nil {
		t.Fatal(err)
	}

	if entries[0].ItemType != GopherTypeText {
		t.Fatal("Type for first line should be text")
	}

	if entries[0].DisplayString != "doc1.txt" {
		t.Fatal("Display string is incorrect for first entry")
	}

	if entries[0].Selector != "doc1.txt" {
		t.Fatal("Selector is incorrect for first entry")
	}

	if entries[0].HostName != "localhost" {
		t.Fatal("Hostname is incorrect for third entry")
	}

	if entries[0].Port != "70" {
		t.Fatal("Port for third entry is incorrect")
	}
}
